use std::fs::File;
use std::io::Read;
use std::process;

use super::USAGE;
use diagnostics::Error;

pub fn read_to_string(path: &str) -> Result<String, Error> {
    let mut file = File::open(path)?;
    let mut buf = Vec::new();
    file.read_to_end(&mut buf)?;
    Ok(String::from_utf8(buf)?)
}

/// Prints usage and exits with bad exit code (-1 for now)
pub fn print_usage() -> ! {
    println!("{}", USAGE);
    process::exit(-1);
}

/// Returns the byte offset of the character after the first eol
pub fn find_after(string: &str, pattern: &str) -> Option<usize> {
    match string.find(pattern) {
        Some(index) => Some(index + pattern.len()),
        None => None,
    }
}

/// Reads from `text` until character `stop` is encountered, then returns a `String` containing
/// `text` from its beginning until character `stop`.
pub fn read_until(text: &str, stop: char) -> String {
    let mut pos = 0;
    while pos < text.len() {
        let ch = match text[pos..].chars().next() {
            Some(c) if c == stop => break,
            Some(c) => c,
            None => break,
        };

        pos += ch.len_utf8();
    }

    String::from(&text[..pos])
}
