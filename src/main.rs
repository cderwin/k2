#![feature(unicode)]

extern crate ansi_term;
extern crate docopt;
extern crate rustc_serialize;
extern crate toml;

#[macro_use]
mod macros;

mod ast;
mod debug;
mod diagnostics;
mod parser;
mod utils;

use std::fs::File;
use std::io::{self, Write};
use std::ops::Range;
use std::process;
use std::rc::Rc;

use docopt::{ArgvMap, Docopt};
use toml::Parser as TomlParser;

use diagnostics::Error;
use parser::ParserResult;

const USAGE: &'static str = "
A server-side templating tool for flexibly generating Makefiles and shell scripts.

Usage:
    k2 [options] <file> ...
    k2 [options]

Options:
    -h, --help                   Print this usage
    -V, --version                Print version info
    -o <file>, --out <file>      File for writing output
    -c <file>, --config <file>   Toml config file
    --debug <target>             Run debug target
";

fn main() {
    let docopt = Docopt::new(USAGE)
        .unwrap_or_else(|e| e.exit())
        .help(true)
        .version(Some("0.1".to_string()));

    let args = docopt.parse().unwrap_or_else(|e| e.exit());

    // Dispatch debug target if given
    let debug_target = args.get_str("--debug");
    let result = match debug_target {
        "" => driver(&args),
        _ => debug::dispatch_target(debug_target, &args)
    };

    if let Err(e) = result {
        println!("{}", e);
        process::exit(2);
    }

    process::exit(0);
}

fn driver(args: &ArgvMap) -> Result<(), Error> {
    // Read config
    let toml_file = args.get_str("--config");
    let toml = if toml_file != "" {
        let contents = utils::read_to_string(toml_file)?;
        let mut toml_parser = TomlParser::new(&contents);
        Some(toml_parser.parse().ok_or(toml_parser.errors)?)
    } else {
        None
    };

    // Open output file (or stdout)
    let mut out: Box<Write> = match args.get_str("--out") {
        "" => Box::new(io::stdout()),
        fname => Box::new(File::open(fname)?)
    };

    // Process files
    for file in args.get_vec("<file>") {
        if file == "" {
            return Ok(());
        }

        let source = Source::from_file(file)?;
        let ast = source.to_ast().map_err(|e| e.into_info(source.clone()))?;
        let output = ast.run();

        out.write_all(output.as_bytes())?;
        out.flush()?;
    }

    Ok(())
}

/// Struct representing a source file
#[derive(Clone, Debug)]
pub struct Source {
    pub path: String,
    pub contents: Rc<String>,
    pub header_range: Range<usize>,
    pub body_range: Range<usize>,
}

impl Source {
    /// Create `Source` from filepath
    pub fn from_file(path: &str) -> Result<Source, Error> {
        let contents = Rc::new(utils::read_to_string(path)?);
        let (header_range, body_range) = Source::parse_contents(path, &contents)?;

        Ok(Source {
            path: path.to_owned(),
            contents: contents,
            header_range: header_range,
            body_range: body_range,
        })
    }

    fn parse_contents(path: &str, contents: &str) -> Result<(Range<usize>, Range<usize>), Error> {
        let null_range = 0..0;

        if !contents.starts_with("===") {
            return Ok((null_range, 0..contents.len()));
        }

        let start_header = utils::find_after(&contents, "\n").ok_or(Error::EmptyFile(path.to_owned()))?;

        let end_header = match contents[start_header..].find("\n===") {
            Some(i) => start_header + i,
            None => return Ok((start_header..contents.len(), null_range))
        };

        let start_body = match utils::find_after(&contents[(end_header + "\n===".len())..], "\n") {
            Some(i) => end_header + "\n===".len() + i,
            None => return Ok((start_header..end_header, null_range)),
        };

        Ok((start_header..end_header, start_body..contents.len()))
    }

    pub fn header(&self) -> &str {
        &self.contents[self.header_range.clone()]
    }

    pub fn body(&self) -> &str {
        &self.contents[self.body_range.clone()]
    }

    /// Generate ast from contents of file
    pub fn to_ast(&self) -> ParserResult<ast::Program> {
        ast::Program::new(self)
    }
}
