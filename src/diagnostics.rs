use std::error;
use std::fmt;
use std::io;
use std::string;

use ansi_term::{ANSIStrings, Style};
use ansi_term::Color;
use parser::Span;
use toml;

use parser::ParserError;
use super::Source;

fn position(body: &str, pos: usize) -> (usize, usize) {
    debug_assert!(pos < body.len());

    let mut line_count = 0;
    let mut char_count = 0;
    for line in body.split('\n') {
        line_count += 1;
        if pos < char_count + line.len() {
            return (line_count, pos - char_count)
        }
        char_count += line.len() + 1;
    }

    unreachable!()
}

/// Error type for all errors in k2
#[derive(Debug)]
pub enum Error {
    Io(io::Error),
    Utf8(string::FromUtf8Error),
    Toml(Vec<toml::ParserError>),
    Info(Info),
    EmptyFile(String),
    NameRedefined(String),
    CyclicDependency,
    NoSuchName(String),
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let err_msg = match *self {
            Error::Io(ref err) => format!("{}", err),
            Error::Utf8(ref err) => format!("{}", err),
            Error::Toml(ref vec) => vec.iter().next().map_or("Unknown Toml error".to_owned(), |err| format!("{}", err)),
            Error::Info(ref info) => return info.fmt(f),
            Error::EmptyFile(ref fname) => format!("File `{}` has no processable body or header", fname),
            Error::NameRedefined(ref s) => format!("Immutable binding `{}` redefined", s),
            Error::CyclicDependency => "A cyclical dependency has been detected.".to_owned(),
            Error::NoSuchName(ref s) => format!("No such name `{}`.", s),
        };
        write!(f, "Error: {}", err_msg);
        Ok(())
    }
}

impl error::Error for Error {
    fn description(&self) -> &str {
        match *self {
            Error::Io(ref err) => err.description(),
            Error::Utf8(ref err) => err.description(),
            Error::Toml(ref vec) => vec.iter().next().map_or("unknown toml error", |err| err.description()),
            Error::Info(..) => "compiler error",
            Error::EmptyFile(..) => "empty file",
            Error::NameRedefined(..) => "name defined more than once",
            Error::CyclicDependency => "cyclical dependency detected",
            Error::NoSuchName(..) => "name not defined",
        }
    }

    fn cause(&self) -> Option<&error::Error> {
        match *self {
            Error::Io(ref err) => Some(err),
            Error::Utf8(ref err) => Some(err),
            Error::Toml(ref vec) => vec.iter().next().map(|err| err as &error::Error),
            Error::Info(ref info) => Some(info),
            _ => None
        }
    }
}

impl From<io::Error> for Error {
    fn from(err: io::Error) -> Error {
        Error::Io(err)
    }
}

impl From<string::FromUtf8Error> for Error {
    fn from(err: string::FromUtf8Error) -> Error {
        Error::Utf8(err)
    }
}

impl From<Vec<toml::ParserError>> for Error {
    fn from(errs: Vec<toml::ParserError>) -> Error {
        Error::Toml(errs)
    }
}

impl From<Info> for Error {
    fn from(info: Info) -> Error {
        Error::Info(info)
    }
}

#[derive(Debug)]
enum Level {
    Warning,
    Error,
}

impl fmt::Display for Level {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        match self {
            &Level::Warning => write!(f, "Warning"),
            &Level::Error => write!(f, "Error")
        }
    }
}

/// Info struct encodes a message to be printed to the user.  Consists of a level (warning or
/// error), the message itself, the source of the error, and the span of the error.
#[derive(Debug)]
pub struct Info {
    level: Level,
    message: String,
    src: Source,
    span: Span,
}

impl Info {
    /// Create an error-level `Info` struct
    pub fn error(msg: String, src: Source, span: Span) -> Info {
        Info {
            level: Level::Error,
            message: msg,
            src: src,
            span: span,
        }
    }

    /// Create a warning-level `Info` struct
    pub fn warning(msg: String, src: Source, span: Span) -> Info {
        Info {
            level: Level::Warning,
            message: msg,
            src: src,
            span: span,
        }
    }

    pub fn build_snippet(&self) -> String {
        let neutral_style = Color::Fixed(12).bold();
        let level_color = match self.level {
            Level::Warning => Color::Fixed(11).bold(),
            Level::Error => Color::Fixed(9).bold()
        };

        let (start_line, start_pos) = position(&self.src.contents, self.span.lo);
        let (end_line, end_pos) = position(&self.src.contents, self.span.lo);

        let margin_size = end_line.to_string().len();
        let empty_margin = neutral_style.paint(format!("{:<n$} |", "", n=margin_size)).to_string();

        let mut snippet = empty_margin.clone();
        let lines = self.src.contents.split('\n').skip(start_line - 1).take(end_line - start_line + 1);

        for (i, line) in lines.enumerate() {
            snippet.push('\n');
            snippet += &neutral_style.paint(format!("{number:<width$} |", number=(start_line + i), width=margin_size)).to_string();
            snippet += &format!(" {}", line);

            // Add highlight line
            snippet.push('\n');
            snippet += &empty_margin;

            let mut highlight_line = String::new();
            for index in 0..line.len() {
                if index >= start_pos && index <= end_pos {
                    highlight_line.push('^');
                } else {
                    highlight_line.push(' ');
                }
            }

            snippet += &level_color.paint(highlight_line).to_string();
        }

        snippet.push('\n');
        snippet + &empty_margin
    }
}

impl fmt::Display for Info {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        let bold_style = Style::new().bold();
        let level_color = match self.level {
            Level::Warning => Color::Fixed(11).bold(),
            Level::Error => Color::Fixed(9).bold()
        };

        let (line_no, char_no) = position(&self.src.contents, self.span.lo);

        let mut msg_line = String::new();
        msg_line += &level_color.paint(format!("{}: ", self.level)).to_string();
        msg_line += &bold_style.paint(format!("{} (Line {}, Char {})\n   {}", self.src.path, line_no, char_no, self.message)).to_string();

        write!(f, "{}\n{}", msg_line, self.build_snippet())
    }
}

impl error::Error for Info {
    fn description(&self) -> &str {
        &self.message
    }
}
