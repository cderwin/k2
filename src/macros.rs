macro_rules! exit {
    ($fmt:expr) => {{
        println!($fmt);
        $crate::utils::print_usage();
    }};
    ($fmt:expr, $($arg:tt)*) => {{
        println!($fmt, $($arg)*);
        $crate::utils::print_usage();
    }}
}
