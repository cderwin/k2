use std::collections::HashMap;

use diagnostics::Error;
use parser::{Parser, ParserResult};
use Source;
use utils;

/// Program is the top node of the ast.  It contains an anonymous macro corresponding to the body
/// of text k2 is to expand and the immediate namespace that will be passed to that macro (defined
/// by the header of the file the body came from).
#[derive(Clone, Debug, RustcEncodable)]
pub struct Program {
    body: Macro,
    context: Namespace
}

impl Program {
    pub fn new(src: &Source) -> ParserResult<Program> {
        let mut parser = Parser::new(src.header())?;
        Ok(Program {
            body: Macro::new(src.body()),
            context: parser.parse()?,
        })
    }

    pub fn run(&self) -> String {
        self.body.run(&self.context)
    }
}

/// Namespace stores all names in a closure in a `HashMap<String, Item>`
#[derive(Clone, Debug, RustcEncodable)]
pub struct Namespace(HashMap<String, Item>);

impl Namespace {
    pub fn new() -> Namespace {
        Namespace(HashMap::new())
    }

    pub fn add_item(&mut self, name: String, item: Item) -> Result<(), Error> {
        if self.0.contains_key(&name) {
            return Err(Error::NameRedefined(name))
        }

        self.0.insert(name, item);
        Ok(())
    }

    pub fn merge(&self, namespace: &Namespace) -> Result<Namespace, Error> {
        let mut result = Namespace(self.0.clone());
        for (key, item) in namespace.0.iter() {
            if result.0.contains_key(key) {
                return Err(Error::NameRedefined(key.to_string()));
            }

            result.0.insert(key.clone(), item.clone());
        }
        Ok(result)
    }

    pub fn get_name(&self, name: &String) -> Option<&Item> {
        self.0.get(name)
    }
}

#[derive(Clone, Debug, RustcEncodable)]
pub enum Item {
    Param(Param),
    Macro(Macro),
}

#[derive(Clone, Debug, RustcEncodable)]
pub struct Param(String);

impl Param {
    pub fn new(value: String) -> Param {
        Param(value)
    }
}

#[derive(Clone, Debug, PartialEq, RustcEncodable)]
pub struct Macro {
    deps: Vec<Expr>,
    template: Vec<Segment>,
}

impl Macro {
    pub fn new(text: &str) -> Macro {
        let template = Macro::parse_definition(text);
        let deps = template.iter()
            .cloned()
            .filter(|seg| seg.is_expr())
            .map(|seg| match seg {
                Segment::Expr(e) => e,
                _ => unreachable!(),
            })
            .collect();

        Macro {
            deps: deps,
            template: template,
        }
    }

    fn parse_definition(text: &str) -> Vec<Segment> {
        let mut result: Vec<Segment> = Vec::new();

        let mut pos = 0;
        while pos < text.len() {
            let segment_str = utils::read_until(&text[pos..], '{');
            pos += segment_str.len() + '{'.len_utf8();
            result.push(Segment::Str(segment_str));

            if pos >= text.len() {
                break;
            }

            let expr_string = utils::read_until(&text[pos..], '}');
            pos += expr_string.len() + '}'.len_utf8();
            result.push(Segment::Expr(Expr(expr_string)));
        }

        result
    }

    /// Check for cyclic dependencies using depth-first search
    pub fn check_deps(&self, namespace: &Namespace) -> Result<(), Error> {
        let mut stack = Vec::new();
        stack.push(self);
        self.check_deps_in(namespace, stack)
    }

    /// For each macro, we check its macro dependencies.  If any of the is on the stack, we return
    /// false.  Else, we push the current macro onto the stack and recursively call check_deps_in.
    /// If the recursive call detects a loop, we return false.  Else returns true.
    fn check_deps_in<'a>(&self, namespace: &'a Namespace, mut stack: Vec<&'a Macro>) -> Result<(), Error> {
        for dep in self.deps.iter() {
            let name = dep.to_string();
            let item = match namespace.get_name(&name) {
                Some(item) => item,
                None => return Err(Error::NoSuchName(name)),
            };

            if let &Item::Macro(ref mac) = item {
                if stack.contains(&mac) {
                    return Err(Error::CyclicDependency);
                }

                stack.push(mac);
                // let result = mac.check_deps_in(namespace, stack);
                // stack.pop();
                //
                // if let Err(..) = result {
                // return Err(namespace::CyclicDependency)
                // }
                //
            }
        }

        Ok(())
    }

    /// Run the macro, returning the output string
    pub fn run(&self, namespace: &Namespace) -> String {
        let mut output = String::new();

        for segment in self.template.iter() {
            match segment {
                &Segment::Str(ref lit) => output.push_str(lit),
                &Segment::Expr(ref name) => {
                    match namespace.get_name(&name.to_string()).unwrap() {
                        &Item::Param(ref param) => output.push_str(&param.0),
                        &Item::Macro(ref mac) => output.push_str(&mac.run(namespace)),
                    }
                }
            }
        }

        output
    }
}

#[derive(Clone, Debug, PartialEq, RustcEncodable)]
struct Expr(String);

impl Expr {
    fn to_string(&self) -> String {
        self.0.clone()
    }
}

#[derive(Clone, Debug, PartialEq, RustcEncodable)]
enum Segment {
    Str(String),
    Expr(Expr),
}

impl Segment {
    fn is_expr(&self) -> bool {
        if let Segment::Expr(_) = *self {
            true
        } else {
            false
        }
    }
}
