use std::char;
use std::ops;

use parser::error::{ParserError, ParserErrorKind, ParserResult};
use Source;

#[derive(Clone, Copy, Debug)]
pub struct Span {
    pub lo: usize,
    pub hi: usize,
}

impl Span {
    pub fn new(lo: usize, hi: usize) -> Span {
        Span {
            lo: lo,
            hi: hi
        }
    }
}

#[derive(Debug, PartialEq)]
pub enum Token {
    Ident(String),
    Literal(String),
    Eq,
    Colon,
    Semicolon,
    Eof,
    Whitespace,
}

/// The lexer converts a `&str` into a stream of tokens
#[derive(Debug)]
pub struct Lexer<'a> {
    pos: usize,
    ch: Option<char>,
    body: &'a str
}

impl<'a> Lexer<'a> {
    pub fn new(text: &'a str) -> Lexer<'a> {
        Lexer {
            pos: 0,
            ch: text.chars().next(),
            body: text,
        }
    }

    fn advance(&mut self) {
        if self.pos < self.body.len() {
            self.pos += self.ch.map_or(0, |c| c.len_utf8());
            self.ch = self.body[self.pos..].chars().next();
        } else {
            self.ch = None;
        }
    }

    fn ch_or_err(&self, start: usize) -> ParserResult<char> {
        self.ch.ok_or(self.err_from(ParserErrorKind::UnexpectedEof, start))
    }

    fn peek(&self) -> Option<char> {
        self.body[self.pos..].chars().nth(1)
    }

    fn peek_or_err(&self, start: usize) -> ParserResult<char> {
        self.peek().ok_or(self.err_from_to(ParserErrorKind::UnexpectedEof, start, 1))
    }

    fn peekpeek(&self) -> Option<char> {
        self.body[self.pos..].chars().nth(2)
    }

    fn err_from<T: Into<ParserErrorKind>>(&self, kind: T, start: usize) -> ParserError {
        ParserError::new(kind, Span::new(start, self.pos))
    }

    fn err_from_to<T: Into<ParserErrorKind>>(&self, kind: T, start: usize, offset: isize) -> ParserError {
        let end = (self.pos as isize) + offset;
        assert!(end >= 0, "Offset longer than text read");
        ParserError::new(kind, Span::new(start, end as usize))
    }

    /// Reads a character from `text`, unescaping it if it is an escape sequence.  `block` and
    /// `line` are boolean flags used to determine whether block literal-specific and/or line
    /// literal-specific escape sequences should apply, as noted below.  Valid escape sequences
    /// are:
    /// ```
    /// \"          double quote (only in line literals)
    /// \{{         double left-bracket (only in block literals)
    /// \}          right-bracket (only in block literals)
    /// \a          bell
    /// \t          tab
    /// \n          newline
    /// \r          carriage return
    /// \v          vertical tab (\x0B)
    /// \\          backslash
    /// \x7F        8-bit hex code
    /// \u7FFF      16-bit unicode escape
    /// \U7EEEFFFF  32-bit unicode escape
    /// ```
    fn read_char(&mut self, block: bool, line: bool) -> ParserResult<String> {
        let start = self.pos;

        let start_ch = self.ch_or_err(start)?;
        if start_ch != '\\' {
            self.advance();
            return Ok(start_ch.to_string());
        }

        // Skip backslash
        self.advance();

        // Handle literal-specific escapes
        if block {
            if let Some(ch) = self.read_block_escape(start)? {
                return Ok(ch.to_owned())
            }
        }

        if line {
            if let Some(ch) = self.read_line_escape(start)? {
                return Ok(ch.to_owned())
            }
        }

        // handle normal escapes
        match self.ch_or_err(start)? {
            '0' => {
                self.advance();
                return Ok("\0".to_owned());
            }

            'n' => {
                self.advance();
                return Ok("\n".to_owned());
            }

            'r' => {
                self.advance();
                return Ok("\r".to_owned());
            }

            't' => {
                self.advance();
                return Ok("\t".to_owned());
            }

            '\\' => {
                self.advance();
                return Ok("\\".to_owned());
            }


            'x' => {
                self.advance();
                self.read_hex_escape(start)
            }

            'u' => {
                self.advance();
                self.read_unicode_escape(start)
            }

            _ => Ok("\\".to_owned())
        }
    }

    /// Assumes `\` has already been read
    fn read_block_escape(&mut self, start: usize) -> ParserResult<Option<&'static str>> {
        let ch = self.peek_or_err(start)?;
        if ch == '{' {
            self.advance();

            if let Some('{') = self.peek() {
                self.advance();
                return Ok(Some("{{"));
            }

            return Ok(Some(r"\"))
        }

        if ch == '}' {
            self.advance();
            return Ok(Some("}"));
        }

        Ok(None)
    }

    /// Assumes `\` has already been read
    fn read_line_escape(&mut self, start: usize) -> ParserResult<Option<&'static str>> {
        let ch = self.peek_or_err(start)?;
        if ch == '"' {
            self.advance();
            return Ok(Some(r#"""#));
        }

        Ok(None)
    }

    /// Assumes `\x` has already been read
    fn read_hex_escape(&mut self, start: usize) -> ParserResult<String> {
        let first = self.ch_or_err(start)?;
        self.advance();

        let second = self.ch_or_err(start)?;
        self.advance();

        let hex_string: String = vec![first, second].iter().cloned().collect();
        let char_code = u32::from_str_radix(&hex_string, 16).map_err(|e| self.err_from(e, start))?;
        let s = char::from_u32(char_code).ok_or(self.err_from(ParserErrorKind::InvalidUnicodeEscape, start))?;

        Ok(s.to_string())
    }

    /// Assumes `\u` has already been read
    fn read_unicode_escape(&mut self, start: usize) -> ParserResult<String> {
        let start = self.pos;

        let ch = self.ch.ok_or(self.err_from(ParserErrorKind::UnexpectedEof, start))?;
        if ch != '{' {
            return Err(self.err_from(ParserErrorKind::InvalidUnicodeEscape, start));
        }

        let mut hex_string = String::new();
        loop {
            let ch = self.ch.ok_or(self.err_from(ParserErrorKind::UnexpectedEof, start))?;
            match ch {
                '}' => break,
                c => hex_string.push(c),
            }
        }

        let char_code = u32::from_str_radix(&hex_string, 16).map_err(|e| self.err_from(e, start))?;
        let s = char::from_u32(char_code).ok_or(self.err_from(ParserErrorKind::InvalidUnicodeEscape, start))?;
        Ok(s.to_string())
    }

    pub fn parsable_token(&mut self) -> ParserResult<(Token, Span)> {
        loop {
            match self.next_token()? {
                (Token::Whitespace, ..) => continue,
                (tok, span) => return Ok((tok, span)),
            }
        }
    }

    pub fn next_token(&mut self) -> ParserResult<(Token, Span)> {
        let start = self.pos;
        let tok = self.try_next_token()?;
        let span = Span::new(start, self.pos);
        Ok((tok, span))
    }

    fn try_next_token(&mut self) -> ParserResult<Token> {
        let c = match self.ch {
            Some(c) => c,
            None => return Ok(Token::Eof),
        };

        // Return whitespace as throwaway token
        if c.is_whitespace() {
            self.advance();
            while self.ch.unwrap_or('\x00').is_whitespace() {
                self.advance()
            }

            return Ok(Token::Whitespace);
        }

        // Lex identifiers
        if c.is_xid_start() {
            let start = self.pos;
            self.advance();

            while self.ch.unwrap_or('\x00').is_xid_continue() {
                self.advance();
            }

            let ident = &self.body[start..self.pos];
            return Ok(Token::Ident(ident.to_string()));
        }

        match c {
            '"' => self.lex_line_literal(),

            '{' => self.lex_block_literal(),

            '=' => {
                self.advance();
                Ok(Token::Eq)
            }

            ':' => {
                self.advance();
                Ok(Token::Colon)
            }

            ';' => {
                self.advance();
                Ok(Token::Semicolon)
            }

            _ => Err(self.err_from(ParserErrorKind::UnexpectedChar, self.pos)),
        }
    }

    fn lex_line_literal(&mut self) -> ParserResult<Token> {
        let start = self.pos;

        assert!(self.ch == Some('"'));
        self.advance();

        let mut lit = String::new();

        while self.ch_or_err(start)? != '"' {
            let string = self.read_char(false, true)?;
            lit.push_str(&string);
        }

        self.advance();
        Ok(Token::Literal(lit))
    }

    fn lex_block_literal(&mut self) -> ParserResult<Token> {
        let start = self.pos;

        assert!(self.ch == Some('{'));
        self.advance();

        let mut lit = String::new();
        let mut count = 1;

        while count > 0 {
            match self.ch_or_err(start)? {
                '{' => {
                    count += 1;
                    lit.push('{');
                    self.advance();
                }

                '}' => {
                    count -= 1;
                    lit.push('}');
                    self.advance();
                }

                _ => {
                    let string = self.read_char(true, false)?;
                    lit.push_str(&string);
                }
            }
        }

        lit.pop();
        lit = lit.trim().to_owned();
        Ok(Token::Literal(lit))
    }
}
