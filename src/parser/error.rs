use std::error;
use std::fmt;
use std::num::ParseIntError;

use diagnostics::Info;
use parser::lexer::Span;
use Source;

pub type ParserResult<T> = Result<T, ParserError>;

#[derive(Clone, Debug)]
pub enum ParserErrorKind {
    InvalidHexEscape(ParseIntError),
    InvalidUnicodeEscape,
    UnexpectedEof,
    UnexpectedChar,
    UnexpectedToken,
}

impl From<ParseIntError> for ParserErrorKind {
    fn from(err: ParseIntError) -> ParserErrorKind {
        ParserErrorKind::InvalidHexEscape(err)
    }
}

#[derive(Clone, Debug)]
pub struct ParserError {
    pub span: Span,
    kind: ParserErrorKind,
}

impl ParserError {
    pub fn new<T: Into<ParserErrorKind>>(cause: T, span: Span) -> ParserError {
        let kind: ParserErrorKind = cause.into();
        ParserError {
            span: span,
            kind: kind,
        }
    }

    pub fn into_info(self, src: Source) -> Info {
        let msg = format!("{}", &self);
        Info::error(msg, src, self.span)
    }
}

impl fmt::Display for ParserError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self.kind {
            ParserErrorKind::InvalidHexEscape(..) => write!(f, "Invalid hex escape"),
            ParserErrorKind::InvalidUnicodeEscape => write!(f, "Invalid unicode escape"),
            ParserErrorKind::UnexpectedEof => write!(f, "Unexpected end of string"),
            ParserErrorKind::UnexpectedChar => write!(f, "Unexpected character"),
            ParserErrorKind::UnexpectedToken => write!(f, "Unexpected token"),
        }
    }
}

impl error::Error for ParserError {
    fn description(&self) -> &str {
        match self.kind {
            ParserErrorKind::InvalidHexEscape(..) => "invalid hex escape",
            ParserErrorKind::InvalidUnicodeEscape => "invalid unicode escape",
            ParserErrorKind::UnexpectedEof => "unexpected end of string",
            ParserErrorKind::UnexpectedChar => "unexpected character",
            ParserErrorKind::UnexpectedToken => "unexpected token",
        }
    }

    fn cause(&self) -> Option<&error::Error> {
        match self.kind {
            ParserErrorKind::InvalidHexEscape(ref err) => Some(err),
            _ => None
        }
    }
}
