use ast;
use parser::lexer::{Lexer, Span, Token};
use parser::error::{ParserError, ParserErrorKind, ParserResult};
use Source;

/// MODULE ::= ITEM * ;
/// ITEM ::= IDENT [ DEF_RHS | MACRO_RHS ] ;
/// DEF_RHS ::= '=' LITERAL ';' ;
/// MACRO_RHS ::= ':' LITERAL ';' ;
/// LITERAL ::= LINE_LITERAL | BLOCK_LITERAL ;
pub struct Parser<'a> {
    lexer: Lexer<'a>,
    token: Token,
    span: Span,
}

impl<'a> Parser<'a> {
    pub fn new(text: &'a str) -> ParserResult<Parser<'a>> {
        let mut lexer = Lexer::new(text);
        let (tok, span) = lexer.parsable_token()?;

        Ok(Parser {
            lexer: lexer,
            token: tok,
            span: span,
        })
    }

    fn unexpected_token(&self) -> ParserError {
        ParserError::new(ParserErrorKind::UnexpectedToken, self.span)
    }

    fn next_token(&mut self) -> ParserResult<()> {
        let (tok, span) = self.lexer.parsable_token()?;
        self.token = tok;
        self.span = span;

        Ok(())
    }

    fn expect(&mut self, expected_token: Token) -> ParserResult<()> {
        if self.token != expected_token {
            return Err(self.unexpected_token())
        }
        self.next_token();
        Ok(())
    }

    fn parse_def(&mut self) -> ParserResult<ast::Param> {
        self.next_token()?;
        let literal = match self.token {
            Token::Literal(ref lit) => lit.clone(),
            _ => return Err(self.unexpected_token()),
        };

        self.next_token()?;
        self.expect(Token::Semicolon)?;
        Ok(ast::Param::new(literal))
    }

    fn parse_macro(&mut self) -> ParserResult<ast::Macro> {
        self.next_token()?;
        let literal = match self.token {
            Token::Literal(ref lit) => lit.clone(),
            _ => return Err(self.unexpected_token()),
        };

        self.next_token()?;
        self.expect(Token::Semicolon)?;
        Ok(ast::Macro::new(&literal))
    }

    pub fn parse(&mut self) -> ParserResult<ast::Namespace> {
        let mut namespace = ast::Namespace::new();
        loop {
            let ident_name = match self.token {
                Token::Ident(ref ident_name) => ident_name.clone(),
                Token::Eof => break,
                _ => return Err(self.unexpected_token()),
            };

            self.next_token()?;
            let result = match self.token {
                Token::Eq => {
                    let item = ast::Item::Param(self.parse_def()?);
                    namespace.add_item(ident_name, item)
                }

                Token::Colon => {
                    let item = ast::Item::Macro(self.parse_macro()?);
                    namespace.add_item(ident_name, item)
                }

                _ => return Err(self.unexpected_token()),
            };
        }

        Ok(namespace)
    }
}
