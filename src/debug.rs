use docopt::ArgvMap;
use rustc_serialize::json;

use std::collections::HashMap;

use ast::Program;
use diagnostics::Error;
use parser::{Lexer, Token};
use super::Source;

macro_rules! define_debug_targets {
    ($($name:expr, $func:path, $help:expr);+) => {

        pub fn dispatch_target(target: &str, args: &ArgvMap) -> Result<(), Error> {
            match target {
                $(
                $name => $func(args),
                )*
                "help" => list_targets(),
                _ => unreachable!()
            }
        }

        fn list_targets() -> Result<(), Error> {
            println!("The following debug targets are defined:\n");
            $(
            println!("    {} -- {}", $name, $help);
            )*

            Ok(())
        }
    }
}

/// Creates the `dispatch_taget` and `list_targets` functions that operate on a list of all debug
/// targets.
define_debug_targets!(
    "print_tokens", print_tokens, "Print the tokens of a program after they have been lexed";
    "json_ast", print_ast, "Print the ast as json"
);

fn print_tokens(args: &ArgvMap) -> Result<(), Error> {
    for file in args.get_vec("<file>") {
        println!("Printing tokens for \"{}\":", file);
        let source = Source::from_file(file)?;
        let mut lexer = Lexer::new(source.header());
        loop {
            let (tok, ..) = lexer.next_token().map_err(|e| e.into_info(source.clone()))?;
            println!("{:?}", tok);

            if tok == Token::Eof {
                break;
            }
        }
    }

    Ok(())
}

fn print_ast(args: &ArgvMap) -> Result<(), Error> {
    let mut file_map: HashMap<&str, Program> = HashMap::new();
    for file in args.get_vec("<file>") {
        let source = Source::from_file(file)?;
        let ast = source.to_ast().map_err(|e| e.into_info(source.clone()))?;
        file_map.insert(file, ast);
    }
    let output = json::encode(&file_map).unwrap();
    println!("{}", output);
    Ok(())
}
