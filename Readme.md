# k2: A Modern Macro Language

While M4 has stood the test of time, it can be unwieldy and verbose compared to
web templating engines.  As an attempt to bridge the gap between templating on
the web and on the command line, I created this tool.  All contributions and
criticisms are welcome.

It is licensed under the MIT License.

## Syntax

The macro definition syntax consist of assignments and macro definitions (together 
called "items").  Assignments set a value of a variable, they are of the format
`{identifier} = {literal};` (ex. `foo = "bar";`).  Macro definitions replaces `=`
with `:` (formally `{identifier}: {literal};`).  However, inside the literals macro expanision can be used: an
identifier wrapped in double brackets (i.e. `{{foo}}`) will be expanded to the
item defined by that identifier (ex. `baz: "foo {{ foo }}";`).

There are two breeds of k2 literals.  Line literals are delimited by `"` and
typically only span one line.  They are the lexical equivalent of string in many
programming languages.  On the other hand, block literals typically span
multiple lines and are used primarily in macro definitions.  They are delimited
by `{` and `}` and stack like parentheses: interior uses of braces must be
matched, and the block literal does not end until the outer brace is matched.
For example, if `foo` and `baz` are defined as above, the block literal 
`{ foo {{ foo }} {{baz}} }` expands to `" foo bar foobar "` instead of ending at the
first `}`.

Identifiers consists of a unicode code point with the property `xid_start`,
followed by zero or more code points with the property `xid_continue`.  For englsih
speakers, this mean identifiers must start with an alphabetic code point or an
underscore, followed by zero or more alphanumeric code points or underscores.

### Formal grammar

The formal EBNF grammar is as follows:

```
CONTEXT   ::= ITEM * ;
ITEM      ::= IDENT [ DEF_RHS | MACRO_RHS ] ;
DEF_RHS   ::= '=' LITERAL ';' ;
MACRO_RHS ::= ':' LITERAL ';' ;
LITERAL   ::= LINE_LITERAL | BLOCK_LITERAL ;
```

Note that both `LINE_LITERAL` and `BLOCK_LITERAL` are lexed as single tokens.

### Escape Sequences

Escape sequences in literals begin with a backslash:
```
\"          double quote (only in line literals)
\{{         double left-bracket (only in block literals)
\}          right-bracket (only in block literals)
\a          bell
\t          tab
\n          newline
\r          carriage return
\v          vertical tab (\x0B)
\\          backslash
\x7F        8-bit hex code
\u7FFF      16-bit unicode escape
\U7EEEFFFF  32-bit unicode escape
```
